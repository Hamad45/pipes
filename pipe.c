#include<stdio.h>
#include<unistd.h>
#include<string.h>

int main()
{
	int fd[2];
	char buff[128];
	pid_t id;
	pipe(fd);
	id=fork();

	if(0 == id)
	{
		printf("child \n");
		close(fd[1]);
		read(fd[0],buff,sizeof(buff));
		printf("parent sent %s: ",buff);
		close(fd[0]);

	}
	else
	{
		printf("parent\n");
		close(fd[0]);
		write(fd[1],"DESD HYDERABAD\n",15);
		close(fd[1]);
	}
	return 0;
}


