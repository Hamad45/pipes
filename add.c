#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>

int main()
{
	int fd[2],d[2],result,num1,num2,sum,val1,val2;
	char buff[128];
	pid_t id;
	pipe(fd);
	pipe(d);
	id=fork();

	if(0 == id)
	{
		close(fd[1]);
		read(fd[0],&num1,sizeof(num1));
		read(fd[0],&num2,sizeof(num2));
		close(fd[0]);
		printf("numbees for addition are %d and %d \n" ,num1,num2);		

		sum=num1+num2;

		close(d[0]);
		write(d[1],&sum,sizeof(sum));
		close(d[1]);
	
	}
	else
	{	
		printf("Enter two numbers\n");
		scanf("%d %d",&val1,&val2);
		close(fd[0]);

		write(fd[1],&val1,sizeof(val1));
		write(fd[1],&val2,sizeof(val2));

		close(fd[1]);

		close(d[1]);
		read(d[0],&result,sizeof(result));
		printf("Result: %d \n",result);
	}
	return 0;
}


