#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
int main(int argc,char const argv[])
{
int fd[2],d[2],num1,num2,val1,val2,sum,result;
pid_t id;
pipe(fd);
pipe(d);
id = fork();

if(id==0)
	{
	printf("child process\n");
	close(fd[1]);
	read(fd[0],&num1,sizeof(num1));
	read(fd[0],&num2,sizeof(num2));
	close(fd[0]);

	sum = num1+num2;

	close(d[0]);
	write(d[1],&sum,sizeof(sum));
	close(d[1]);
	}
	else
	{
	printf("parent process\n");
	printf("enter the two values for sum");
	scanf("%d%d",&val1,&val2);
	close(fd[0]);
	write(fd[1],&val1,sizeof(val1));
	write(fd[1],&val2,sizeof(val2));
	close(fd[1]);
	
	close(d[1]);
	read(d[0],&result,sizeof(result));
	close(d[0]);
	printf("sum =%d",result);
	}
	return 0;
}
