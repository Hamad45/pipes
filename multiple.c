#include<stdio.h>
#include<unistd.h>
#include<string.h>

int main()
{
	int fd[2],d[2];
	char buff[128],msg1[128],msg2[128];
	pid_t id1,id2;
	pipe(fd);
	pipe(d);
	id1=fork();

	if(0 == id1)
	{
//		printf("child 1 \n");
		close(fd[1]);
		read(fd[0],buff,sizeof(buff));
		printf("parent sent: %s\n",buff);
		close(fd[0]);
	}

	else
	{	
		id2 = fork();
		if(0==id2)
		{
//			printf("child 2 \n");
			close(fd[1]);
			read(d[0],buff,sizeof(buff));
			close(d[0]);
			printf("parent sent: %s\n",buff);
		}
		
		else
		{
			printf("Enter message for child1 and child2 \n");
			scanf("%s %s",&msg1,&msg2);
//			printf("parent\n");
			close(fd[0]);
			write(fd[1],&msg1,sizeof(msg1));
			close(fd[1]);		
			close(d[0]);
			write(d[1],&msg2,sizeof(msg2));
			close(d[1]);

		}
	}
	return 0;
}


